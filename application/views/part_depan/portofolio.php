<?php
    if($data->num_rows > 0):
?>
<div id="fh5co-grid-products" class="animate-box" style="padding: 10rem 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>Lihat Produk Kami</h2>
                <p>Kami terlah mengerjakan banyak project.</p>
            </div>
        </div>
    </div>
    <?php
        foreach ($data->result_array() as $i) :
            $port_id		= $i['port_id'];
            $port_judul		= $i['port_judul'];
            $port_deskripsi	= $i['port_deskripsi'];
            $port_author	= $i['port_author'];
            $port_image		= $i['port_image'];
            $port_tglpost	= $i['tanggal'];
    ?>
    <div class="col-md-3">
        <a href="#">
            <img src="<?= base_url() . 'assets/images/' . $port_image; ?>" class="img-responsive">
        </a>
        <div class="v-align">
            <div class="v-align-middle" style="padding-top: 2rem;">
                <h3 class="title"><?php echo $port_judul;?></h3>
                <h5 class="category">Web Application</h5>
            </div>
        </div>
    </div>
</div>
<?php 
        endforeach;
    else:
?>
<div id="fh5co-grid-products" class="animate-box" style="padding: 10rem 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>Lihat Produk Kami</h2>
                <p>Maaf, tapi belum ada produk tersedia.</p>
            </div>
        </div>
    </div>
</div>
<?php
    endif;
?>