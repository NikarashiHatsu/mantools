<div class="container" style="padding: 10rem 0;">
	<div class="row"><br/>
		<h2 class="text-center">Galeri ManTools</h2>
		<?php
			foreach ($data->result() as $row):
		?>
		<div class="col-md-4">
			<a class="example-image-link" href="<?= base_url() . 'assets/images/' . $row->galeri_gambar; ?>" data-lightbox="example-2" data-title="<?php echo $row->galeri_judul;?>">
				<img class="example-image img-responsive" src="<?= base_url() . 'assets/images/' . $row->galeri_gambar; ?>" alt="image-1" />
			</a>
		</div>
		<?php
			endforeach;
		?>
		</div>
	</div>
</div>