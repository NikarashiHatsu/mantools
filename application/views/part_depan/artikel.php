<?php
    if($post->num_rows > 0):
?>
<div id="fh5co-blog" class="animate-box">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>Artikel Terbaru</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
			<?php
                foreach ($post->result_array() as $j) :
                    $post_id        = $j['tulisan_id'];
                    $post_judul     = $j['tulisan_judul'];
                    $post_isi       = $j['tulisan_isi'];
                    $post_author    = $j['tulisan_author'];
                    $post_image     = $j['tulisan_gambar'];
                    $post_tglpost   = $j['tanggal'];
                    $post_slug      = $j['tulisan_slug'];
			?>
            <div class="col-md-4">
                <a class="fh5co-entry" href="<?= base_url() . 'artikel/'.$post_slug;?>">
                    <figure>
                        <img src="<?= base_url() . 'assets/images/'.$post_image;?>" alt="" class="img-responsive">
                    </figure>
                    <div class="fh5co-copy">
                        <h3>
                            <?= $post_judul; ?>
                        </h3>
                        <span class="fh5co-date">
                            <?= $post_tglpost . ' | ' . $post_author; ?>
                        </span>
                        <?= mb_strimwidth($post_isi, 0, 20, '...'); ?>
                    </div>
                </a>
            </div>
            <?php
                endforeach;
            ?>
            <div class="col-md-12 text-center">
                <p><a href="<?= base_url() . 'artikel'?>" class="btn btn-primary btn-outline with-arrow">View More <i class="icon-arrow-right"></i></a></p>
            </div>
        </div>
    </div>
</div>
<?php
    else:
?>
<div id="fh5co-blog" class="animate-box">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>Belum ada artikel</h2>
            </div>
        </div>
    </div>
</div>
<?php
    endif;
?>