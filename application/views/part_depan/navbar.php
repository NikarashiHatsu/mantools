<div id="fh5co-page">
<header id="fh5co-header" role="banner">
	<div class="container">
		<div class="header-inner">
			<h1><a href="<?= base_url().''?>">ManTools</a></h1>
			<nav role="navigation">
				<ul>
					<li><a href="<?= base_url().''?>">Beranda</a></li>
					<li><a href="<?= base_url().'about'?>">Tentang</a></li>
					<li><a href="<?= base_url().'portfolio'?>">Portfolio</a></li>
					<li><a href="<?= base_url().'artikel'?>">Blog</a></li>
					<li><a href="<?= base_url().'gallery'?>">Galeri</a></li>
					<li><a href="<?= base_url().'kontak'?>">Kontak</a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>