<aside id="fh5co-hero" class="js-fullheight">
    <div class="flexslider js-fullheight">
        <ul class="slides">
            <li style="background-image: url(<?= 'theme/images/slide_1.jpeg'?>);">
                <div class="overlay-gradient"></div>
                <div class="container">
                    <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                        <div class="slider-text-inner">
                            <h2>Mulai Imajinasimu</h2>
                            <p><a href="#" class="btn btn-primary btn-lg">Mulai</a></p>
                        </div>
                    </div>
                </div>
            </li>
            <li style="background-image: url(<?= 'theme/images/slide_2.jpg'?>);">
                <div class="container">
                    <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                        <div class="slider-text-inner">
                            <h2>Ambil Ambisimu</h2>
                            <p><a href="#" class="btn btn-primary btn-lg">Mulai</a></p>
                        </div>
                    </div>
                </div>
            </li>
            <li style="background-image: url(<?= 'theme/images/slide_3.jpg'?>);">
                <div class="container">
                    <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                        <div class="slider-text-inner">
                            <h2>Kami Berpikir Berbeda Dari Yang Lain</h2>
                            <p><a href="#" class="btn btn-primary btn-lg">Mulai</a></p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>