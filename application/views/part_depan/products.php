<div id="fh5co-why-us" class="animate-box">
	<div class="container">
		<div class="row">
			<div class="col-md-4 text-center item-block">
				<span class="icon"><img src="<?= base_url() . 'theme/images/30.svg'?>" class="img-responsive"></span>
				<h3>Desain Grafis</h3>
				<p class="text-justify">Bangun identitas bisnis anda di dunia Internet melaui Desain Logo, Desain Kemasan, Desain Produk, dll.</p>
				<p><a href="<?= base_url() . 'portfolio'?>" class="btn btn-primary btn-outline with-arrow">Pelajari Selengkapnya <i class="icon-arrow-right"></i></a></p>
			</div>
			<div class="col-md-4 text-center item-block">
				<span class="icon"><img src="<?= base_url() . 'theme/images/18.svg'?>" class="img-responsive"></span>
				<h3>Promosi Digital</h3>
				<p class="text-justify">Iklankan produk anda di dunia Internet yang luas dengan bentuk <i>Multimedia Digital Advertising</i>.</p>
				<p><a href="<?= base_url() . 'portfolio'?>" class="btn btn-primary btn-outline with-arrow">Pelajari Selengkapnya <i class="icon-arrow-right"></i></a></p>
			</div>
			<div class="col-md-4 text-center item-block">
				<span class="icon"><img src="<?= base_url() . 'theme/images/27.svg'?>" class="img-responsive"></span>
				<h3>Desain Website</h3>
				<p>Bangun identitas bisnis dan usaha anda di dunia Internet melalui Website.</p>
				<p><a href="<?= base_url() . 'portfolio'?>" class="btn btn-primary btn-outline with-arrow">Pelajari Selengkapnya <i class="icon-arrow-right"></i></a></p>
			</div>
		</div>
	</div>
</div>