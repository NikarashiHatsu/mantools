<div class="fh5co-section-with-image">
    <img src="<?= base_url() . 'theme/images/image_1.jpg'?>" alt="" class="img-responsive">
    <div class="fh5co-box animate-box">
        <h2>Berpikir Maju</h2>
        <p>Kami selalu berusaha membangun dunia Multimedia dengan teknologi terbaru.</p>
        <p><a href="<?= base_url() . 'portfolio'?>" class="btn btn-primary btn-outline with-arrow">Mulai <i class="icon-arrow-right"></i></a></p>
    </div>
</div>