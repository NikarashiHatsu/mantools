<footer id="fh5co-footer" role="contentinfo">
	<div class="container">
		<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
			<h3>Tentang Kami</h3>
			<p>Agensi ini merupakan perusahaan yang bergerak dan memfokuskan diri pada bidang Konsultan IT dan Desain.</p>
		</div>
		<div class="col-md-6 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
			<h3>Layanan Kami</h3>
			<ul class="float">
				<li><a href="<?= base_url() . 'portfolio'?>">Desain Grafis</a></li>
				<li><a href="<?= base_url() . 'portfolio'?>">Konsultan IT</a></li>
				<li><a href="<?= base_url() . 'portfolio'?>">Perangkat Lunak & Program</a></li>
			</ul>
			<ul class="float">
				<li><a href="<?= base_url() . 'portfolio'?>">Layanan SEO</a></li>
				<li><a href="<?= base_url() . 'portfolio'?>">Perangkat Keras & Jaringan</a></li>
				<li><a href="<?= base_url() . 'portfolio'?>">Pemasaran Daring</a></li>
			</ul>
		</div>
		<div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
			<h3>Ikuti Kami</h3>
			<ul class="fh5co-social">
				<li><a href="#"><i class="icon-twitter"></i></a></li>
				<li><a href="#"><i class="icon-facebook"></i></a></li>
				<li><a href="#"><i class="icon-google-plus"></i></a></li>
				<li><a href="#"><i class="icon-instagram"></i></a></li>
			</ul>
		</div>
		<div class="col-md-12 fh5co-copyright text-center">
			<p>&copy; <?= getdate()['year'] == 2019 ? 2019 : "2019 - " . getdate()['year']; ?> by <a href="<?php echo base_url() . ''?>" target="_blank">ManTools</a>. All Rights Reserved.</p>
		</div>
	</div>
</footer>