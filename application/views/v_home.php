<!DOCTYPE html>
<html class="no-js">
	<?php require_once "part_depan/header.php"; ?>
	<body>
		<?php 
			require_once 'part_depan/navbar.php';
			require_once 'part_depan/slideshow.php';
			require_once 'part_depan/products.php';
			require_once 'part_depan/optional_hero.php';
			require_once 'part_depan/artikel.php';
			$this->load->view('v_footer');

			require_once 'part_depan/javascript.php';
		?>
	</body>
</html>
