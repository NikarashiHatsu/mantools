
<!DOCTYPE html>
<html class="no-js">
	<?php
		require_once "part_depan/header.php";
		require_once "part_depan/navbar.php";
	?>
	<aside id="fh5co-hero" clsas="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
				<li style="background-image: url(<?= base_url().'theme/images/slide_3.jpg'?>);">
					<div class="overlay-gradient"></div>
					<div class="container">
						<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
							<div class="slider-text-inner">
								<h2>Selalu terhubung.</h2>
								<p class="fh5co-lead">Kami siap melayani Anda.</p>
							</div>
						</div>
					</div>
				</li>
		  	</ul>
	  	</div>
	</aside>
	<div class="fh5co-contact animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h3>Contact Info.</h3>
					<ul class="contact-info">
						<li>
							<i class="icon-map"></i>
							--Alamat Fix--
						</li>
						<li>
							<i class="icon-phone"></i>
							--Nomor Telepon Fix--
						</li>
						<li>
							<i class="icon-envelope"></i>
							<a href="<?= base_url() . ''?>">
								--Email Fix--
							</a>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="<?= base_url() . ''?>">
								--Website Fix--
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-8 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
					<?= $this->session->flashdata('msg');?>
					<div class="row">
						<form method="post" action="<?= base_url().'kontak/kirim_pesan'?>">
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control" name="nama" placeholder="Nama" type="text" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input class="form-control" name="email" placeholder="Email" type="email" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<textarea name="pesan" class="form-control" id="" cols="30" rows="7" placeholder="Message" required></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input value="Kirim Pesan" class="btn btn-primary" type="submit">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <div id="map" class="animate-box" data-animate-effect="fadeIn"></div> -->
	<!-- Google Map API, bakal dipake klo Website udah fix -->
	<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script> -->

	<?php
		$this->load->view('v_footer');
		require_once "part_depan/javascript.php";
	?>
	</body>
</html>
