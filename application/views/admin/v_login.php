<!DOCTYPE html>
<html>
	<?php
		require_once "part/header.php";
	?>
<body class="hold-transition login-page">
	<div class="login-box">
		<div>
			<p>
				<?= $this->session->flashdata('msg'); ?>
			</p>
		</div>
		<div class="login-box-body">
			<p style="font-weight: bolder; font-size: 20px;" class="login-box-msg">
				ManTools Agency
			</p>
			<hr style="margin-top: 0;" 	/>
			<form action="<?= base_url() . 'administrator/auth' ?>" method="post">
				<div class="form-group has-feedback">
					<input type="text" name="username" class="form-control" placeholder="Username" required>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password" required>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8" style="padding-left: 0;">
						<div class="checkbox icheck">
							<label>
								<input type="checkbox"> Ingat Saya
							</label>
						</div>
					</div>
					<div class="col-xs-4" style="padding-top: 0.5rem;">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
					</div>
				</div>
			</form>
			<hr style="margin: 1rem 0;" />
			<p class="text-center">&copy; <?= getdate()['year'] == 2019 ? 2019 : "2019 - " . getdate()['year']; ?> by <a href="<?php echo base_url() . ''?>" target="_blank">ManTools</a>. All Rights Reserved.</p>
		</div>
	</div>
	<?php
		require_once "part/javascript.php";
	?>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%'
			});
		});
	</script>
</body>
</html>
