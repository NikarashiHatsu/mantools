<head>
    <title>ManTools Agency</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="M Fikri Setiadi" />
    <link rel="shorcut icon" type="text/css" href="<?= base_url() . 'assets/images/favicon.png'?>">
	<link rel="stylesheet" href="<?= base_url().'assets/plugins/iCheck/square/blue.css'?>">
	<link rel="stylesheet" href="<?= base_url().'assets/dist/css/AdminLTE.min.css'?>">
	<link rel="stylesheet" href="<?= base_url().'assets/font-awesome/css/font-awesome.min.css'?>">
	<link rel="stylesheet" href="<?= base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
    <script src="<?= base_url() . 'theme/js/modernizr-2.6.2.min.js'?>"></script>
</head>