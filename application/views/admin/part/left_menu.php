  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/dashboard" ? "active" : ""); ?>">
          <a href="<?php echo base_url().'admin/dashboard'?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview <?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/tulisan/add_tulisan" ? "active" : ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/tulisan" ? "active" : ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/kategori" ? "active" : ""))); ?>">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>Post</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/tulisan/add_tulisan" ? "active" : ""); ?>"><a href="<?php echo base_url().'admin/tulisan/add_tulisan'?>"><i class="fa fa-thumb-tack"></i> Add New</a></li>
            <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/tulisan" ? "active" : ""); ?>"><a href="<?php echo base_url().'admin/tulisan'?>"><i class="fa fa-list"></i> Post Lists</a></li>
            <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/kategori" ? "active" : ""); ?>"><a href="<?php echo base_url().'admin/kategori'?>"><i class="fa fa-wrench"></i> Kategori</a></li>
          </ul>
        </li>

        <li class="treeview <?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/portfolio/add_portfolio" ? "active" : ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/portfolio" ? "active" : "")); ?>">
          <a href="#">
            <i class="fa fa-code"></i>
            <span>Portfolio</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/portfolio/add_portfolio" ? "active" : ""); ?>"><a href="<?php echo base_url().'admin/portfolio/add_portfolio'?>"><i class="fa fa-thumb-tack"></i> Add Portfolio</a></li>
            <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/portfolio" ? "active" : ""); ?>"><a href="<?php echo base_url().'admin/portfolio'?>"><i class="fa fa-list"></i> Portfolio List</a></li>
          </ul>
        </li>

        <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/pengguna" ? "active" : ""); ?>">
          <a href="<?php echo base_url().'admin/pengguna'?>">
            <i class="fa fa-users"></i> <span>Pengguna</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
       
        <li class="treeview <?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/album" ? "active" : ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/galeri" ? "active" : "")); ?>">
          <a href="#">
            <i class="fa fa-camera"></i>
            <span>Gallery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/album" ? "active" : ""); ?>"><a href="<?php echo base_url().'admin/album'?>"><i class="fa fa-clone"></i> Album</a></li>
            <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/galeri" ? "active" : ""); ?>"><a href="<?php echo base_url().'admin/galeri'?>"><i class="fa fa-picture-o"></i> Photos</a></li>
          </ul>
        </li>

        <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/komentar" ? "active" : ""); ?>">
          <a href="<?php echo base_url().'admin/komentar'?>">
            <i class="fa fa-comment"></i> <span>Komentar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_komentar;?></small>
            </span>
          </a>
        </li>
        
        <li class="<?= ($_SERVER['REDIRECT_QUERY_STRING'] == "/admin/inbox" ? "active" : ""); ?>">
          <a href="<?php echo base_url().'admin/inbox'?>">
            <i class="fa fa-envelope"></i> <span>Inbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_pesan;?></small>
            </span>
          </a>
        </li>

         <li>
          <a href="<?php echo base_url().'administrator/logout'?>">
            <i class="fa fa-sign-out"></i> <span>Sign Out</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>