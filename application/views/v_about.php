<!DOCTYPE html>
<html class="no-js">
	<?php require_once "part_depan/header.php"; ?>
	<body>
		<?php 
			require_once 'part_depan/navbar.php';
		?>
		<aside id="fh5co-hero" class="js-fullheight">
			<div class="flexslider js-fullheight">
				<ul class="slides">
				<li style="background-image: url(<?= base_url().'theme/images/slide_3.jpg'?>);">
					<div class="overlay-gradient"></div>
					<div class="container">
						<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
							<div class="slider-text-inner">
								<h2>Siapa Kami</h2>
								<p class="fh5co-lead">Sebuah agensi yang bergerak di bidang Multimedia.</a></p>
							</div>
						</div>
					</div>
				</li>
				</ul>
			</div>
		</aside>
		
		<div class="fh5co-about animate-box">
			<div class="col-md-6 col-md-offset-3 fh5co-heading">
				<h2 class="text-center">Tentang Kami</h2>
				<p class="text-justify">Mantools merupakan agensi yang bergerak dan memfokuskan diri pada bidang Multimedia. Seiring dengan pesatnya perkembangan teknologi dan keterkaitan nya dengan bidang usaha maka kami hadir di dunia teknologi informasi untuk memberikan solusi, perencanaan, dan strategi yang terintegerasi sebagai nilai tambah yang maksimal bagi kebutuhan dan permasalahan dibidang Multimedia.</p>
			</div>
			<div class="container">
				<div class="col-md-6">
					<figure>
						<img src="<?= base_url().'theme/images/image_1.jpg'?>" alt="Free HTML5 Template" class="img-responsive">
					</figure>
				</div>
				<div class="col-md-6">
					<h3 style="margin-bottom: 1rem;">Visi</h3>
					<ul style="padding-left: 1.5rem; margin-bottom: 3rem;">
						<li>Menjadi Perusahaan IT Profesional dengan solusi dan layanan yang optimal serta memiliki daya saing.</li>
						<li>Memberikan Layanan dan Solusi yang terintegerasi dan mengikuti perkembangan dunia Teknologi Informasi.</li>
					</ul>
					<h3 style="margin-bottom: 1rem;">Misi</h3>
					<ul style="padding-left: 1.5rem;">
						<li>Tidak hanya memberi solusi, kami memberikan layanan yang terpadu dalam setiap layanan Teknologi Informasi yang kami berikan.</li>
						<li>Memberikan produk dan layanan yang berkualitas dengan layanan purna jual yang maksimal kepada setiap pelangan kami.</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="fh5co-team animate-box">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
						<h2>Tim ManTools</h2>
						<p>Kami memiliki tim yang solid. <i>One team, one spirit, and one goal.</i></p>
					</div>
					<div class="col-md-4 fh5co-staff" style="margin-bottom: 5rem;">
						<img src="<?= base_url().'theme/images/person3.jpg'?>" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Hermanto Kusnendar</h3>
						<h4>Founder</h4>
						<p>--Deskripsi Hermanto--</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 fh5co-staff" style="margin-bottom: 5rem;">
						<img src="<?= base_url().'theme/images/person2.jpg'?>" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Tubagus Naufal Hidayat</h3>
						<h4>Co-Founder</h4>
						<p>--Deskripsi Tubagus Naufal Hidayat--</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 fh5co-staff" style="margin-bottom: 5rem;">
						<img src="<?= base_url().'theme/images/person2.jpg'?>" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Aghits Nidallah</h3>
						<h4>Administrator</h4>
						<p>--Deskripsi Aghits Nidallah--</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 fh5co-staff" style="margin-bottom: 5rem;">
						<img src="<?= base_url().'theme/images/person2.jpg'?>" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Risky Setiawan</h3>
						<h4>Administrator</h4>
						<p>--Deskripsi Risky Setiawan--</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 fh5co-staff" style="margin-bottom: 5rem;">
						<img src="<?= base_url().'theme/images/person4.jpg'?>" alt="Free HTML5 Bootstrap Template" class="img-responsive">
						<h3>Tubagus Sajiwo</h3>
						<h4>Marketing Staff</h4>
						<p>--Deskripsi Tubagus Sajiwo--</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-google"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php
			require_once 'part_depan/products.php';
			$this->load->view('v_footer');
		?>
	</div>
	<?php
		require_once "part_depan/javascript.php";
	?>
	</body>
</html>
