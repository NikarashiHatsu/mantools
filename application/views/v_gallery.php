<!DOCTYPE html>
<html class="no-js">
	<?php
		require_once "part_depan/header.php";
		require_once "part_depan/navbar.php";
	?>
	<aside id="fh5co-hero" clsas="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
				<li style="background-image: url(<?php echo base_url().'theme/images/slide_3.jpg'?>);">
					<div class="overlay-gradient"></div>
					<div class="container">
						<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
							<div class="slider-text-inner">
								<h2>Galeri Kami</h2>
								<p class="fh5co-lead">Beberapa hasil foto <a href="<?= base_url() . ''?>">ManTools Agency</a></p>
							</div>
						</div>
					</div>
				</li>
		  	</ul>
	  	</div>
	</aside>
	<?php
		require_once "part_depan/galeri.php";

		$this->load->view('v_footer');
		require_once "part_depan/javascript.php";
	?>
	</body>
</html>
